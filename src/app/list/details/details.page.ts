import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Participante } from 'src/app/shared/models/participante';
import { ParticipanteService } from 'src/app/shared/services/participante.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  id: any;
  participante: Participante = {} as Participante;

  constructor(
    private activeRoute: ActivatedRoute,
    private navController: NavController,
    private participanteService: ParticipanteService
  ) { }

  ngOnInit() {
    this.id = this.activeRoute.snapshot.params['id'];
    if (this.id) {
      this.participanteService.getParticipanteLocal(this.id).then(participante => {
        this.participante = participante;
      });
    }
  }

  saveParticipante() {
    if (this.id) {
      console.log('já existe', this.id);
      this.participanteService.updateParticipanteLocal(this.participante).then(response => {
        console.log(response);
        this.navController.back();
      }).catch(e => {
        console.log(e);
      });
    } else {
      this.participanteService.insereParticipanteLocal(this.participante).then(response => {
        this.participanteService.saveParticipante(this.participante).then(response => {
          this.navController.back();
        });
      }).catch(e => {
        console.log(e);
      });
    }
  }

}
