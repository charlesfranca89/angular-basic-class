import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Participante } from '../shared/models/participante';
import { ParticipanteService } from '../shared/services/participante.service';
import { ComponentDidLoad } from 'ionicons/dist/types/stencil.core';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  public participantes: Array<Participante> = [];
  constructor(
    // Controla navegação
    private router: Router,
    private participanteService: ParticipanteService
  ) {
  }

  ionViewWillEnter() {
    this.loadParticipantes();
  }

  navigateToDetails(id) {
    if (id) {
      this.router.navigateByUrl(`list/details/${id}`);
    } else {
      this.router.navigateByUrl(`list/details`);
    }
  }

  loadParticipantes() {
    this.participanteService.getParticipantesLocal().then(participantes => {
      this.participantes = participantes;
    });
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
