import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public name = '<strong>Charles</strong>';
  div2ativa = false;
  div1ativa = true;

  model = {
    name: '',
    username: '',
    password: ''
  };

  isAdmin = false;

  constructor() {
    this.model.name = 'Charles';
    this.model.username = 'Charles';
    this.model.password = 'asdadsa';
  }

  changeName() {
    this.name = 'New Name';
  }

  submit() {
    console.log(this.model);
  }

}
