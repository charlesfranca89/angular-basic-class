export interface Participante {
    id: string;
    sigla: string;
    matricula: string;
    nascimento: Date;
}
